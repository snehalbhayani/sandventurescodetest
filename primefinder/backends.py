from django.core.cache import caches
from datetime import datetime as dt
import datetime
import traceback
# Caching framework is used to save prime numbers larger than a certain value
# so that subsequent requests don't have to compute again. For calculating a prime number 
# we shall use the cache to find the greatest available prime number.

overflow_factor=100000
def  find_prime(number):
    cache=caches['default']
    existing_prime=cache.get(number)
    start_time = dt.now()
    if existing_prime != None: # If the prime number at this index already exists, we pull it and return it straightaway.
        return [number,existing_prime]
    try:
        gt_prime_indx=cache.get('greatest_prime_indx',1)
        greatest_prime=int(cache.get(gt_prime_indx,1))
# We initialize this to 2 as this is the smallest prime number we want to save in cache by default.
        smallest_prime_indx=cache.get('smallest_prime_indx',2) 
        x=1
        indx_buffer=gt_prime_indx
        prime_buffer=0
        timeout=False
        while indx_buffer<number:            
            while True:
                prime_buffer=greatest_prime+x
                is_prime=True
                for y in range(2,indx_buffer+1): # Given a number to check for its primeness we iterate through all the prime numbers less than it 
                    if y < smallest_prime_indx:  # and greater than the prime at the key 'smallest_prime_indx'. This is due to the overflow window
                        numerator=y              # setting above and to ensure the cache doesn't overflow. 
                    else:
                        numerator=cache.get(y)
                    if prime_buffer%numerator ==0:
                        is_prime=False
                        break
                if is_prime: # Check for whether the number is prime or not.
                    indx_buffer=indx_buffer+1
                    if indx_buffer-smallest_prime_indx+1 >overflow_factor   : # Here we evict the cache,
                        cache.delete(smallest_prime_indx)                     # as we have explicitly set the cache to hold only 100000 objects. 
                        smallest_prime_indx=smallest_prime_indx+1
                        cache.set('smallest_prime_indx',smallest_prime_indx)
                    cache.add(indx_buffer,prime_buffer)    
                    x=x+1
                    break  
                x=x+1
            now_time=dt.now()
            print (datetime.timedelta(seconds=10)+start_time - now_time)
            
            if datetime.timedelta(seconds=10)+start_time < now_time:
                print ('Timeout while computing. So returning partial result.')
                timeout=True
                break

        if timeout is True:
            cache.set('greatest_prime_indx',indx_buffer) # We update the greatest prime number after we get a timeout 
        else:
            cache.set('greatest_prime_indx',number) # if there is not timeout we just update the actual result
        return [indx_buffer, prime_buffer]                      # we update the index of the greatest prime number existing inside the cache.

    except:
        print ( traceback.format_exc())
        return [indx_buffer, None]
