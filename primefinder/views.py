from django.http import *
from django.views.generic import View
import json
from django.shortcuts import render
from forms import NumberForm
import logging, traceback
from backends import find_prime
logger = logging.getLogger(__name__)


# We use Class based view. Basically, the app is ajax based as finding a prime number may take some time. Hence I have integrated ajax post call with django. The core logic of finding nth prime number is put in a separate module, backend.py.
class PrimeNumberView(View):
    def get(self, request):
        template_name='prime_number_form.html'
        context={'prime_number':'Enter a Number!'}
        return render(request,template_name,context)

    def post(self,request):
        template_name = 'prime_number_form.html'
        post_dict=request.POST
        form_from_params=NumberForm(data=post_dict)
        if request.is_ajax():
            logger.info('Ajax request for finding the nth prime number has been received. ')
            if form_from_params.is_valid():        
                cleaned_data=form_from_params.cleaned_data
                number=int(cleaned_data['number'])
                results=find_prime(number)            
                returned_number=results[0]
                prime_number=results[1]# replace by the prime number and the returned index that we have found
                number=returned_number
                logger.info(prime_number)
                print returned_number, prime_number
            else:
                number=post_dict['number']            
                logger.error(form_from_params.errors.as_json()) 
                prime_number='Try Again, Number is invalid!'

            context={'number':number,'prime_number':prime_number}
            return HttpResponse(json.dumps(context), content_type='application/json')




